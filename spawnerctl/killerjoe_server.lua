local killerJoe = peripheral.wrap("left")

rednet.open("front")

while true do
	local stack = killerJoe.getStackInSlot(1)
	local data = {stack.display_name, stack.dmg, stack.max_dmg}

	rednet.broadcast(textutils.serialize(data))

	sleep(5)
end

rednet.close("front")

