local path = "/spawnerctl"
local files = {
	"Button",
	"SpawnerController",
	"glasses",
	"install",
	"killerjoe_client",
	"killerjoe_server",
	"spawnerapi",
	"spawnerapp",
	"spawnerctl",
	"spawnerserv",
	"spawnersys",
	"utils"
}

if path ~= "" and not fs.exists(path) then
	fs.makeDir(path)
end

for _, value in pairs(files) do
	write("Copying file '" .. value .. "'... ")

	local request = http.get("https://gitlab.com/Quent42340/ccraft_scripts/raw/master/spawnerctl/" .. value .. ".lua");
	-- TODO: Check errors
	local f = fs.open(path .. "/" .. value, "w")
	f.write(request.readAll())
	f.close()
	print("Done. (" .. request.getResponseCode() .. ")")
	request.close();
end

shell.run(path .. "/spawnerctl install")

