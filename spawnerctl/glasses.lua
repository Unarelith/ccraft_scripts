spawner_glasses = function()
	local glasses = peripheral.wrap("bottom")

	while true do
		id, message = rednet.receive()

		if id == 27 then
			data = textutils.unserialise(message)

			glasses.clear()

			glasses.addText(2, 35, data[1] .. ": " .. data[3] - data[2] .. "/" .. data[3])

			glasses.sync()
		end
	end
end

